<?php

namespace Tripcloud\Dotpay;

/**
 * Tripcloud\Dotpay\Callback
 */
class Callback
{
    const TRUSTED_IP = '195.150.9.37';

    const STATUS_NEW = 1;
    const STATUS_DONE = 2;
    const STATUS_REJECTED = 3;
    const STATUS_REFUND = 4;
    const STATUS_COMPLAINT = 5;

    /**
     * @var string
     */
    protected $trustedIp;

    /**
     * @var string
     */
    protected $pin;

    /**
     * @var string
     */
    protected $clientIp;

    /**
     * @var int
     */
    protected $accountId;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $paymentId;

    /**
     * @var string
     */
    protected $transactionId;

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var string
     */
    protected $currency = 'PLN';

    /**
     * @var float
     */
    protected $originalAmount;

    /**
     * @var string
     */
    protected $originalCurrency;

    /**
     * @var int
     */
    protected $paymentMethod;

    /**
     * @var string
     */
    protected $payerEmail;

    /**
     * @var string
     */
    protected $service;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $transactionStatus;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $securityHash;

    /**
     * @var string
     */
    protected $merchantInfo;

    /**
     * @var string
     */
    protected $merchantEmail;

    /**
     * @var string
     */
    protected $dateTime;

    /**
     * Class constructor
     *
     * @param string $pin
     * @param string $clientIp
     * @param string $trustedIp
     */
    public function __construct($pin, $clientIp, $trustedIp = self::TRUSTED_IP)
    {
        $this->pin = $pin;
        $this->clientIp = $clientIp;
        $this->trustedIp = $trustedIp;
    }

    /**
     * Getter of AccountId
     *
     * @return int
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Setter of AccountId
     *
     * @param int $accountId
     *
     * @return static
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;

        return $this;
    }

    /**
     * Getter of Status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Setter of Status
     *
     * @param string $status
     *
     * @return static
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Getter of PaymentId
     *
     * @return string
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * Setter of PaymentId
     *
     * @param string $paymentId
     *
     * @return static
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * Getter of TransactionId
     *
     * @return string
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Setter of TransactionId
     *
     * @param string $transactionId
     *
     * @return static
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * Getter of Amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Setter of Amount
     *
     * @param float $amount
     *
     * @return static
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Getter of Currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Setter of Currency
     *
     * @param string $currency
     *
     * @return static
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Getter of OriginalAmount
     *
     * @return float
     */
    public function getOriginalAmount()
    {
        return $this->originalAmount;
    }

    /**
     * Setter of OriginalAmount
     *
     * @param float $originalAmount
     *
     * @return static
     */
    public function setOriginalAmount($originalAmount)
    {
        $this->originalAmount = $originalAmount;

        return $this;
    }

    /**
     * Getter of OriginalCurrency
     *
     * @return string
     */
    public function getOriginalCurrency()
    {
        return $this->originalCurrency;
    }

    /**
     * Setter of OriginalCurrency
     *
     * @param string $originalCurrency
     *
     * @return static
     */
    public function setOriginalCurrency($originalCurrency)
    {
        $this->originalCurrency = $originalCurrency;

        return $this;
    }

    /**
     * Getter of PaymentMethod
     *
     * @return int
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Setter of PaymentMethod
     *
     * @param int $paymentMethod
     *
     * @return static
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Getter of PayerEmail
     *
     * @return string
     */
    public function getPayerEmail()
    {
        return $this->payerEmail;
    }

    /**
     * Setter of PayerEmail
     *
     * @param string $payerEmail
     *
     * @return static
     */
    public function setPayerEmail($payerEmail)
    {
        $this->payerEmail = $payerEmail;

        return $this;
    }

    /**
     * Getter of Service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Setter of Service
     *
     * @param string $service
     *
     * @return static
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Getter of Username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Setter of Username
     *
     * @param string $username
     *
     * @return static
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Getter of Code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Setter of Code
     *
     * @param string $code
     *
     * @return static
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Getter of Password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Setter of Password
     *
     * @param string $password
     *
     * @return static
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Getter of TransactionStatus
     *
     * @return string
     */
    public function getTransactionStatus()
    {
        return $this->transactionStatus;
    }

    /**
     * Setter of TransactionStatus
     *
     * @param string $transactionStatus
     *
     * @return static
     */
    public function setTransactionStatus($transactionStatus)
    {
        $this->transactionStatus = $transactionStatus;

        return $this;
    }

    /**
     * Getter of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Setter of Description
     *
     * @param string $description
     *
     * @return static
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Getter of SecurityHash
     *
     * @return string
     */
    public function getSecurityHash()
    {
        return $this->securityHash;
    }

    /**
     * Setter of SecurityHash
     *
     * @param string $securityHash
     *
     * @return static
     */
    public function setSecurityHash($securityHash)
    {
        $this->securityHash = $securityHash;

        return $this;
    }

    /**
     * Getter of MerchantInfo
     *
     * @return string
     */
    public function getMerchantInfo()
    {
        return $this->merchantInfo;
    }

    /**
     * Setter of MerchantInfo
     *
     * @param string $merchantInfo
     *
     * @return static
     */
    public function setMerchantInfo($merchantInfo)
    {
        $this->merchantInfo = $merchantInfo;

        return $this;
    }

    /**
     * Getter of MerchantEmail
     *
     * @return string
     */
    public function getMerchantEmail()
    {
        return $this->merchantEmail;
    }

    /**
     * Setter of MerchantEmail
     *
     * @param string $merchantEmail
     *
     * @return static
     */
    public function setMerchantEmail($merchantEmail)
    {
        $this->merchantEmail = $merchantEmail;

        return $this;
    }

    /**
     * Getter of DateTime
     *
     * @return string
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * Setter of DateTime
     *
     * @param string $dateTime
     *
     * @return static
     */
    public function setDateTime($dateTime)
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    /**
     * Set callback data from array
     * i.e. Callback->setPostData($_POST)
     *
     * @param array $data
     */
    public function setPostData(array $data)
    {
        $this->accountId = isset($data['id']) ? $data['id'] : null;
        $this->status = isset($data['status']) ? $data['status'] : null;
        $this->paymentId = isset($data['control']) ? $data['control'] : null;
        $this->transactionId = isset($data['t_id']) ? $data['t_id'] : null;
        $this->amount = isset($data['amount']) ? $data['amount'] : null;
        $this->currency = isset($data['currency']) ? $data['currency'] : 'PLN';
        if (!empty($data['orginal_amount'])) {
            list($this->originalAmount, $this->originalCurrency) = explode(' ', $data['orginal_amount'], 2);
        }
        $this->paymentMethod = isset($data['channel']) ? $data['channel'] : null;
        $this->payerEmail = isset($data['email']) ? $data['email'] : null;
        $this->service = isset($data['service']) ? $data['service'] : null;
        $this->code = isset($data['code']) ? $data['code'] : null;
        $this->username = isset($data['username']) ? $data['username'] : null;
        $this->password = isset($data['password']) ? $data['password'] : null;
        $this->transactionStatus = isset($data['t_status']) ? $data['t_status'] : null;
        $this->description = isset($data['description']) ? $data['description'] : null;
        $this->securityHash = isset($data['md5']) ? $data['md5'] : null;
        $this->merchantInfo = isset($data['p_info']) ? $data['p_info'] : null;
        $this->merchantEmail = isset($data['p_email']) ? $data['p_email'] : null;
        $this->dateTime = isset($data['t_date']) ? $data['t_date'] : null;
    }

    /**
     * Check if callback is valid
     *
     * @return bool
     */
    public function isValid()
    {
        return $this->isValidIp() && $this->isValidData();
    }

    /**
     * Check if client ip is valid
     *
     * @return bool
     */
    protected function isValidIp()
    {
        return self::TRUSTED_IP === $this->clientIp;
    }

    /**
     * Check if callback data is valid
     *
     * @return bool
     */
    protected function isValidData()
    {
        $data = array(
            $this->pin,
            $this->accountId,
            $this->paymentId,
            $this->transactionId,
            $this->amount,
            $this->payerEmail,
            $this->service,
            $this->code,
            $this->username,
            $this->password,
            $this->transactionStatus,
        );

        return $this->securityHash === hash('md5', implode(':', $data));
    }
}
