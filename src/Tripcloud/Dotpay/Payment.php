<?php

namespace Tripcloud\Dotpay;

/**
 * Tripcloud\Dotpay\Payment
 */
class Payment
{
    const ENDPOINT_PROD = 'https://ssl.dotpay.pl/';
    const ENDPOINT_TEST = 'https://ssl.dotpay.pl/test_payment/';

    const TYPE_RETURN_BUTTON = 0;
    const TYPE_URL_POST = 1;
    const TYPE_RETURN_AND_POST = 3;
    const TYPE_SKIP_DOTPAY = 4;

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var int
     */
    protected $accountId;

    /**
     * @var string
     */
    protected $paymentId;

    /**
     * @var string
     */
    protected $language;

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var string
     */
    protected $currency;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $payerName;

    /**
     * @var string
     */
    protected $payerSurname;

    /**
     * @var string
     */
    protected $payerEmail;

    /**
     * @var string
     */
    protected $payerCountry;

    /**
     * @var string
     */
    protected $successUrl;

    /**
     * @var int
     */
    protected $paymentMethod;

    /**
     * @var bool
     */
    protected $forcePaymentMethod = false;

    /**
     * @var string
     */
    protected $callbackUrl;

    /**
     * @var string
     */
    protected $buttonText;

    /**
     * @var int
     */
    protected $type = self::TYPE_RETURN_BUTTON;

    /**
     * Class constructor
     *
     * @param string $endpoint
     */
    public function __construct($endpoint = self::ENDPOINT_PROD)
    {
        $this->endpoint = $endpoint;
    }

    /**
     * Getter of Endpoint
     *
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * Setter of Endpoint
     *
     * @param string $endpoint
     *
     * @return static
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    /**
     * Getter of AccountId
     *
     * @return int
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * Setter of AccountId
     *
     * @param int $accountId
     *
     * @return static
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;

        return $this;
    }

    /**
     * Getter of PaymentId
     *
     * @return string
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * Setter of PaymentId
     *
     * @param string $paymentId
     *
     * @return static
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * Getter of Amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Setter of Amount
     *
     * @param float $amount
     *
     * @return static
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Getter of Currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Setter of Currency
     *
     * @param string $currency
     *
     * @return static
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Getter of Language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Setter of Language
     *
     * @param string $language
     *
     * @return static
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Getter of PaymentMethod
     *
     * @return int
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Setter of PaymentMethod
     *
     * @param int $paymentMethod
     *
     * @return static
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Getter of ForcePaymentMethod
     *
     * @return bool
     */
    public function isForcePaymentMethod()
    {
        return $this->forcePaymentMethod;
    }

    /**
     * Setter of ForcePaymentMethod
     *
     * @param bool $forcePaymentMethod
     *
     * @return static
     */
    public function setForcePaymentMethod($forcePaymentMethod)
    {
        $this->forcePaymentMethod = $forcePaymentMethod;

        return $this;
    }

    /**
     * Getter of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Setter of Description
     *
     * @param string $description
     *
     * @return static
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Getter of PayerName
     *
     * @return string
     */
    public function getPayerName()
    {
        return $this->payerName;
    }

    /**
     * Setter of PayerName
     *
     * @param string $payerName
     *
     * @return static
     */
    public function setPayerName($payerName)
    {
        $this->payerName = $payerName;

        return $this;
    }

    /**
     * Getter of PayerSurname
     *
     * @return string
     */
    public function getPayerSurname()
    {
        return $this->payerSurname;
    }

    /**
     * Setter of PayerSurname
     *
     * @param string $payerSurname
     *
     * @return static
     */
    public function setPayerSurname($payerSurname)
    {
        $this->payerSurname = $payerSurname;

        return $this;
    }

    /**
     * Getter of PayerEmail
     *
     * @return string
     */
    public function getPayerEmail()
    {
        return $this->payerEmail;
    }

    /**
     * Setter of PayerEmail
     *
     * @param string $payerEmail
     *
     * @return static
     */
    public function setPayerEmail($payerEmail)
    {
        $this->payerEmail = $payerEmail;

        return $this;
    }

    /**
     * Getter of PayerCountry
     *
     * @return string
     */
    public function getPayerCountry()
    {
        return $this->payerCountry;
    }

    /**
     * Setter of PayerCountry
     *
     * @param string $payerCountry
     *
     * @return static
     */
    public function setPayerCountry($payerCountry)
    {
        $this->payerCountry = $payerCountry;

        return $this;
    }

    /**
     * Getter of SuccessUrl
     *
     * @return string
     */
    public function getSuccessUrl()
    {
        return $this->successUrl;
    }

    /**
     * Setter of SuccessUrl
     *
     * @param string $successUrl
     *
     * @return static
     */
    public function setSuccessUrl($successUrl)
    {
        $this->successUrl = $successUrl;

        return $this;
    }

    /**
     * Getter of CallbackUrl
     *
     * @return string
     */
    public function getCallbackUrl()
    {
        return $this->callbackUrl;
    }

    /**
     * Setter of CallbackUrl
     *
     * @param string $callbackUrl
     *
     * @return static
     */
    public function setCallbackUrl($callbackUrl)
    {
        $this->callbackUrl = $callbackUrl;

        return $this;
    }

    /**
     * Getter of Type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Setter of Type
     *
     * @param int $type
     *
     * @return static
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Getter of ButtonText
     *
     * @return string
     */
    public function getButtonText()
    {
        return $this->buttonText;
    }

    /**
     * Setter of ButtonText
     *
     * @param string $buttonText
     *
     * @return static
     */
    public function setButtonText($buttonText)
    {
        $this->buttonText = $buttonText;

        return $this;
    }

    /**
     * Get Data for payment
     *
     * @return array
     */
    public function getData()
    {
        return array_filter(
            array(
                'id'          => $this->accountId,
                'control'     => $this->paymentId,
                'amount'      => $this->amount,
                'currency'    => $this->currency,
                'lang'        => $this->language,
                'description' => $this->description,
                'firstname'   => $this->payerName,
                'lastname'    => $this->payerSurname,
                'country'     => $this->payerCountry,
                'email'       => $this->payerEmail,
                'channel'     => $this->paymentMethod,
                'ch_lock'     => (int) $this->forcePaymentMethod,
                'URLC'        => $this->callbackUrl,
                'URL'         => $this->successUrl,
                'buttontext'  => $this->buttonText,
                'type'        => $this->type,
            )
        );
    }

    /**
     * Get URL form payment
     *
     * @return string
     */
    public function getUrl()
    {
        return sprintf(
            "%s?%s",
            $this->endpoint,
            http_build_query($this->getData())
        );
    }
}
